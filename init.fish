# according to: https://github.com/fish-shell/fish-shell/issues/1774
if type -q rhash
    set -gx RHASH_VERSION (rhash --version)
else
     echo "Please install rhash first. See https://github.com/rhash/RHash"
     exit 1
end
